import Vue from "vue";
import App from "./App.vue";
// import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import './assets/styles.css';
// import 'bootstrap-icons'


Vue.config.productionTip = false;
// Vue.prototype.$handleErrors = function(err) {
  // console.log(err.response.data.message);
//   if (err.response.data.message == "Token Expired") {
//     this.errorMessage =
//       "زمان نشست شما به پایان رسیده است ، لطفا مجددا وارد شوید";
//   } else if (err.response.data.message == "Questionnaire has no question") {
//     this.errorMessage = "هیچ پرسشی یافت نشد ";
//   } else if (err.response.data.message == "Password Not Verified") {
//     this.errorMessage = "رمز عبور اشتباه است ";
//   } else if (err.response.data.message == "User not found.") {
//     this.errorMessage = "کاربر مورد نظر یافت نشد";
//   } else if ((err.response.data.message = "Password does not match")) {
//     this.errorMessage = "تکرار رمز عبور صحیح نمی باشد";
//   } else {
//     this.errorMessage = err.response.data.message;
//   }
//   return this.errorMessage;
// };


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
