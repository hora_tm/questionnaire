import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    userAnswer: {
      questionnaireId: 0,
      questionNumber: 0,
      type: "",
      chooseOptions: [],
      desAnsw: "",
    },
    userAnswerCounter: 0,
    username: "",
  },
  plugins: [createPersistedState()],
  mutations: {},
  actions: {},
  modules: {},
});
